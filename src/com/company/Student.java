package com.company;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by traed on 3/23/2017.
 */
public class Student {
    ArrayList<Class> Classes;
    int Credits;
    String ClassStatus;

    public void Save() {
        PrintWriter writer = null;
        try {
            writer = new PrintWriter("studRex.dat", "UTF-8");

            for (Class c : Classes) {
                writer.println(c.CourseCode + "#" + c.credits + "#" + c.Grade);
            }
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Class> getClasses() {
        return Classes;
    }

    public void setClasses(ArrayList<Class> classes) {
        Classes = classes;
    }

    public int getCredits() {
        return Credits;
    }

    public void setCredits(int credits) {
        Credits = credits;
    }

    public String getClassStatus() {
        return ClassStatus;
    }

    public void setClassStatus(String classStatus) {
        ClassStatus = classStatus;
    }

    public Student() {
        Credits = 0;
        this.Classes = new ArrayList<>();
        try {

            File StudData = new File("studRex.dat");
            Scanner input = new Scanner(StudData);

            while (input.hasNextLine()) {

                String line = input.nextLine();
                String[] items = line.split("#");
                this.Classes.add(
                        new Class(items[0], Integer.parseInt(items[1]), items[2])
                );
            }
            input.close();
            for (Class c : Classes) {
                Credits += c.credits;
            }
            ClassStatus = "";// add if statements to assign year freshman sophomore ect.

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
