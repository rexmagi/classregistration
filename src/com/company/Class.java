package com.company;

import java.util.ArrayList;

/**
 * Created by traed on 3/23/2017.
 */
public class Class {
    String CourseCode;
    int credits;
    String Grade;
    String ReqioredGrade;
    ArrayList<String> PreRecs;

    public Class(String courseCode, int credits, String reqioredGrade, ArrayList<String> preRecs) {
        CourseCode = courseCode;
        this.credits = credits;
        ReqioredGrade = reqioredGrade;
        PreRecs = preRecs;
    }

    public String getReqioredGrade() {
        return ReqioredGrade;
    }

    public void setReqioredGrade(String reqioredGrade) {
        ReqioredGrade = reqioredGrade;
    }

    public Class(String courseCode, int credits, String grade) {


        CourseCode = courseCode;
        this.credits = credits;
        Grade = grade;
    }

    public String getGrade() {
        return Grade;
    }

    public void setGrade(String grade) {
        Grade = grade;
    }

    public String getCourseCode() {

        return CourseCode;
    }

    public void setCourseCode(String courseCode) {
        CourseCode = courseCode;
    }

    public int getCredits() {
        return credits;
    }

    public void setCredits(int credits) {
        this.credits = credits;
    }

    public ArrayList<String> getPreRecs() {
        return PreRecs;
    }

    public void setPreRecs(ArrayList<String> preRecs) {
        PreRecs = preRecs;
    }


}
