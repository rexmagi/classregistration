package com.company;

import javax.swing.*;
import java.io.File;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        String choice = "";
        ArrayList<Class> Checksheet = new ArrayList<>();
        Student stud = new Student();
        Startup(Checksheet);
        while (!choice.equals("0")) {
            choice = JOptionPane.showInputDialog(null, "1) Current Status \n" +
                    "2) Next Course term planning\n" +
                    "3) Current terms grades\n" +
                    "0) Exit", "RMU Semester Advising", JOptionPane.QUESTION_MESSAGE);
            switch (choice) {
                case "1":
                    DisplayStatus(stud);
                    break;
                case "2":
                    NextTermPlanning(stud, Checksheet);
                    break;
                case "3":
                    EnterCurrentGrades(stud);
                    break;
                default:
                    JOptionPane.showMessageDialog(null, "Enter Valid Option", "Error", JOptionPane.WARNING_MESSAGE);
            }


        }
        Shutdown(stud);
    }

    private static void EnterCurrentGrades(Student stud) {
        String Message = "Select Class to Enter Grade for\n";
        String choice = "";
        while (!choice.equals(Integer.toString(stud.getClasses().size()))) {
            for (int i = 0; i < stud.getClasses().size(); i++) {
                Message += Integer.toString(i) + ") " + stud.getClasses().get(i).getCourseCode() + "\n";
            }
            Message+=Integer.toString(stud.getClasses().size())+") Exit \n";
            choice = JOptionPane.showInputDialog(null, Message, "RMU Semester Advising", JOptionPane.INFORMATION_MESSAGE);
            if (Integer.parseInt(choice) < stud.getClasses().size()) {
                String grade = JOptionPane.showInputDialog(null, "Enter Grade for " + stud.getClasses().get(Integer.parseInt(choice)).getCourseCode(), "Enter Grade",JOptionPane.INFORMATION_MESSAGE);
                // check that they entered a grade
                // put grade into the proper class
            }
        }
    }

    private static void NextTermPlanning(Student stud, ArrayList<Class> checksheet) {
        boolean hasAllGrades = true;
        for (Class c : stud.getClasses()) {
            if ((c.Grade == null || c.Grade.isEmpty()) && hasAllGrades) {
                hasAllGrades = false;
            }
        }
        if (hasAllGrades) {
            int totalcredits = 0;
            ArrayList<Class> nextTermSchedule = new ArrayList<>();
            for (Class c : checksheet) {
                if (NeedToRetake(c, stud) && totalcredits + c.credits <= 18) {
                    totalcredits += c.credits;
                    nextTermSchedule.add(c);
                }
                if (meetPrerecs(c, stud.getClasses()) && totalcredits + c.credits <= 18) {
                    totalcredits += c.credits;
                    nextTermSchedule.add(c);
                }
            }
            // display the contents fo nextTermSchedule
            // remove any courses the student needs to retake then add all to list or only add classes the student needs to take fresh
            // clear grade for course retaking
            stud.getClasses().addAll(nextTermSchedule);
        } else {
            JOptionPane.showMessageDialog(null, "Student has not entered grades for all courses.\n Please enter all grades for all classes that you are registered for.", "RMU Next Term Planning", JOptionPane.WARNING_MESSAGE);
        }

    }

    private static boolean NeedToRetake(Class c, Student stud) {
        boolean retake = false;
        for (Class takenCourcse: stud.getClasses()) {
            if(takenCourcse.getCourseCode().equals(c.CourseCode))
                if(c.getReqioredGrade() != null)// check to see if grade is less than or equal to needed grade
                    retake = true;
        }
        return retake;
    }

    private static boolean meetPrerecs(Class c, ArrayList<Class> classes) {
        boolean canTake = true;
        for (String prerec : c.getPreRecs()) {
            boolean Inlist = false, Hasgrade = false;
            for (Class TakenClass : classes) {
                if (TakenClass.getCourseCode().equals(prerec)) {
                    Inlist = true;
                    if (TakenClass.getGrade() != null && !TakenClass.getGrade().isEmpty())// check to see if grade is greater than needed grade
                        Hasgrade = true;
                }
            }
            if (!Inlist && !Hasgrade)
                canTake = false;
        }
        return canTake;
    }

    private static void DisplayStatus(Student stud) {
        String Message = "";
        Message += "Current Credits: " + stud.getCredits() + "\n";
        Message += "Current Class: " + stud.ClassStatus + "\n";
        // add message about number of remaining classes or credits needed
        JOptionPane.showMessageDialog(null, Message, "Current Status", JOptionPane.INFORMATION_MESSAGE);

    }

    private static void Startup(ArrayList<Class> checksheet) {
        Hashtable<String, ArrayList<String>> PreRecLookup = new Hashtable<>();
        Scanner input;
        try {
            File PreRecFile = new File("PreRecs.dat");
            input = new Scanner(PreRecFile);

            while (input.hasNextLine()) {

                String line = input.nextLine();
                String[] items = line.split(":");
                PreRecLookup.put(items[0], new ArrayList<>((Arrays.asList(items[1].split(",")))));
            }
            input.close();


            File CheeksheetFile = new File("Checksheet.dat");
            input = new Scanner(CheeksheetFile);

            while (input.hasNextLine()) {

                String line = input.nextLine();
                String[] items = line.split("#");
                checksheet.add(
                        new Class(items[0], Integer.parseInt(items[1]), items[2], PreRecLookup.get(items[0]))
                );
            }
            input.close();
        } catch (Exception e) {
        }
    }

    private static void Shutdown(Student stud) {
        stud.Save();
    }
}